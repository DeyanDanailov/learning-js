function sendMessage (message) {
    window.InputEvent = window.Event || window.InputEvent;

    let event = new InputEvent('input', {
        bubbles: true
    });

    let textbox = document.querySelector("#main > footer > div.vR1LG._3wXwX.copyable-area > div._2A8P4 > div > div._2_1wd.copyable-text.selectable-text");

    textbox.textContent = message;
    textbox.dispatchEvent(event);

    document.querySelector("#main > footer > div.vR1LG._3wXwX.copyable-area > div:nth-child(3) > button").click();
}


const { Client } = require('whatsapp-web.js');
const client = new Client();

client.on('qr', (qr) => {
    // Generate and scan this code with your phone
    console.log('QR RECEIVED', qr);
});
client.on('ready', () => {
    console.log('Client is ready!');

    // Number where you want to send the message.
    const number = "+911234567890";

    // Your message.
    const text = "Hey john";

    // Getting chatId from the number.
    // we have to delete "+" from the beginning and add "@c.us" at the end of the number.
    const chatId = number.substring(1) + "@c.us";

    // Sending message.
    client.sendMessage(chatId, text);
});