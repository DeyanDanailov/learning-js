
// const log = require('./logger');
//
// log('message');

// const path = require('path');
// let pathObj = path.parse(__filename);
//
// console.log(pathObj);

// const os = require('os');
// let totalMemory = os.totalmem();
// let freeMemory = os.freemem();
//
// console.log('Total memory: ' + totalMemory);
//
// // Template string
// // ES6 / ES2015 / ECMAScript 6
// console.log(`Free memory: ${freeMemory}`);
//
// const fs = require('fs');
//
// // const files = fs.readdirSync('./');
// // console.log(files);
//
// fs.readdir('./', function (err, files){
//     if (err) console.log('Error', err);
//     else console.log('Result', files);
// });

// const EventEmitter = require('events');
// //let message = 'Dido is a dude';
//
//
// const Logger = require('./logger');
// const logger = new Logger();
//
// // Register a listener
// logger.on('messageLogged', (arg) => { // e, eventArg
//     console.log('Listener called', arg);
// });
// logger.on('logging', (arg) => {
//     console.log(`${arg.data} is being logged`);
// });
//
// logger.log('Hello dudes')


const http = require('http');

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.write('Hello dudes');
        res.end();
    }

    if (req.url === '/api/courses') {
        res.write(JSON.stringify([1, 2, 3]))
        res.end();
    }
});

server.listen(3000);

console.log('Listening on port 3000...');