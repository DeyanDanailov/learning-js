const EventEmitter = require('events');

let url = 'http://mylogger.io/log';

class Logger extends EventEmitter {

    log(message) {
        // Send an HTTP request

        this.emit('logging', {data: message});

        // Raise an event
        this.emit('messageLogged', {id: 1, url: 'https//...'});

        console.log(message);
    }
}



module.exports = Logger;