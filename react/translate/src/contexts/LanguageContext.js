import React, {useState} from "react";
const Context = React.createContext('english');

export const LanguageStore = (props) => {
    const [language, setLanguage] = useState('dutch');
    const [color, setColor] = useState('green');

    const onLanguageChange = language => {
        setLanguage(language);
    }

    return (
        <Context.Provider value={{language,color, onLanguageChange}}>
            {props.children}
        </Context.Provider>
    )
}

export default Context;