import React, {useContext} from "react";
import LanguageContext from "../contexts/LanguageContext";
import ColorContext from "../contexts/ColorContext";

const Button = () => {
    const {language, color} = useContext(LanguageContext);
    const text = language === 'english' ? 'Submit' : 'Voorleggen';
    //const color = useContext(ColorContext);

    return (
        <button className={` ui button ${color}`}>
            {text}
        </button>
    )

    // const renderSubmit = (value) => {
    //     return value === 'english' ? 'Submit' : 'Voorleggen'
    // }

    // const renderButton = (color) => {
    //     return (
    //         <button className={` ui button ${color}`}>
    //             <LanguageContext.Consumer>
    //                 {value => renderSubmit(value)}
    //             </LanguageContext.Consumer>
    //         </button>
    //     )
    // }
    //
    // return (
    //     <ColorContext.Consumer>
    //         {(color) => renderButton(color)}
    //     </ColorContext.Consumer>
    //
    // );
};

export default Button;