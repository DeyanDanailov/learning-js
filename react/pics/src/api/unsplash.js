import axios from 'axios';

export default axios.create( {
    baseURL: 'api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 4_zW6w-NOiTlTR3fxEnvDs6SwZj9Qo-yr9djjaidECk',
    }
})