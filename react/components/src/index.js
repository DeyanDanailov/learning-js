import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import {faker} from '@faker-js/faker';
import './style/App.css';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <div>
                    <h4>Warning!</h4>
                    Are you sure you want to do this?
                </div>
            </ApprovalCard>


            <ApprovalCard>
                <CommentDetail author="Sam" timeAgo="Today at 4:14PM" avatar={faker.image.avatar()} text="Hello"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Al" timeAgo="Today at 6:14PM" avatar={faker.image.avatar()} text="Great"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Jim" timeAgo="Today at 8:14PM" avatar={faker.image.avatar()}
                               text="Go fuck yourself"/>
            </ApprovalCard>

        </div>
    );
};

ReactDOM.render(
    <App/>
    , document.querySelector('#root'));
