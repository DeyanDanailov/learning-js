// const http = require('http');
// const fs = require('fs');
// const path = require('path')
// let manifest = require('./manifest.json')
//
// let filePath = path.join(__dirname, 'index.js');
// let stat = fs.statSync(filePath);
//
//
// console.log('server listening on port 3060...')
// http.createServer(function (req, res) {
//     req.on('data', chunk => {
//         let data = JSON.parse(chunk)
//         if (data.info === "getManifest") {
//             console.log('should give Manifest')
//             res.end(JSON.stringify(manifest))
//         } else if (data.info === 'getFiles') {
//             console.log('should send files')
//             res.writeHead(200, {
//                 'Content-Type': 'text',
//                 'Content-Length': stat.size
//             });
//             let readStream = fs.createReadStream(filePath);
//             readStream.pipe(res);
//         }
//     })
//
// }).listen(3060);

const static = require('node-static');
const http = require('http');

const file = new(static.Server)(__dirname);

http.createServer(function (req, res) {
    file.serve(req, res);
}).listen(3060);
console.log('server listening on port 3060...')
