function ABVRegistrator()
{

let randFirstName = '';
let randLastName = '';
let randUsername = '';
let randomPass = '';
let randomPhoneNum = '8';

function randomNum(startNum, endNum){
    let randNum = startNum + Math.floor(Math.random() * (endNum - startNum))
    return randNum;
}

function generateFirstName(){
    let firstNames = ["pesho", "gosho", "gancho", "mitko", "dobri", "ilcho", "ivcho"];
    randFirstName = firstNames[Math.floor(Math.random() * (firstNames.length))];
    return randFirstName;
}
randFirstName = generateFirstName();

function generateLastName(){
    let lastNames = ["peshov", "goshov", "ganchov", "mitkov", "dobrev", "ilchov", "ivchov"];
    randLastName = lastNames[Math.floor(Math.random() * (lastNames.length))];
    return randLastName;
}
randLastName = generateLastName();

let concatSymbols = [".", "_", "-", "+"];
let randSymbol = concatSymbols[Math.floor(Math.random() * (concatSymbols.length))];
randUsername = randFirstName + randSymbol + randLastName;

function generatePassword() {
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < 12; i++ ) {
        randomPass += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return randomPass;
 }
 randomPass = generatePassword();
 
 function generatePhoneNum() {
    for ( let i = 0; i < 8; i++ ) {
        randomPhoneNum += randomNum(0,9);
    }
    return randomPhoneNum;
 }
 randomPhoneNum = generatePhoneNum();

 $("#regformUsername").val(randUsername);
 $("#password").val(randomPass);
 $("#password2").val(randomPass);
 $("#mobilePhone").val(randomPhoneNum);

 firstNameCapitalized = randFirstName.charAt(0).toUpperCase() + randFirstName.slice(1)
 lastNameCapitalized = randLastName.charAt(0).toUpperCase() + randLastName.slice(1)

 $("#fname").val(firstNameCapitalized);
 $("#lname").val(lastNameCapitalized);

 let genders = document.querySelectorAll('div.abv-radio label');
 randGender = genders[Math.floor(Math.random() * (genders.length))];
 randGender.click();


document.querySelector("#bday").value = randomNum(1, 28);
document.querySelector("#bmonth").value = randomNum(1, 12);
document.querySelector("#byear").value = randomNum(1970, 2001)

}
ABVRegistrator();


