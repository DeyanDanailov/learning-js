class Upgrade {
    constructor(name,startingPrice,cps) {
        this.name = name;
        this.startingPrice=startingPrice;
        this.singleCPS = cps;
        this.count = 0
    }

    getPrice(){
        return this.startingPrice*(Math.pow(game.multiplier,this.count));
    }

    canBuy(){
        game.coins = parseFloat(document.getElementById('counterNumber').innerText);
        return this.getPrice() <= game.coins;
    }

    get cps(){
        return this.singleCPS*this.count;
    }

    buyOne(upgradeIndex){
        game.coins = game.coins - this.getPrice();
        document.getElementById('counterNumber').innerText = game.coins.toFixed(2).toString();
        this.count++;
        document.getElementById('cps').innerText = (game.cps += this.cps).toFixed(2).toString();
        document.getElementById('upgrade' + upgradeIndex.toString() + 'price').innerText = this.getPrice().toFixed(2).toString();
        document.getElementById('upgrade' + upgradeIndex.toString() + 'count').innerText = this.count.toString();

    }
}


let game = {
    coins:0.0,
    cps:1.0,
    upgrades:[
        new Upgrade('squirrel', 10,0.1),
        new Upgrade('rabbit', 20,0.2),
        new Upgrade('rhino', 30,0.3),
        new Upgrade('cobra', 40,0.4),
        new Upgrade('goose', 50,0.5),
        new Upgrade('crab', 60,0.6),
        new Upgrade('wolf', 70,0.7),
        new Upgrade('scorpion', 80,0.8),
        new Upgrade('bear', 90,0.9),
        new Upgrade('tiger', 100,1.0)
    ],
    multiplier:1.618
}

function tryToBuy(upgradeIndex) {
    if (game.upgrades[upgradeIndex].canBuy()) {
       game.upgrades[upgradeIndex].buyOne(upgradeIndex);
    } else  {
        alert("Not enough coins!");
    }
}
function IncreaseCookies (){
    game.coins = parseFloat(document.getElementById('counterNumber').innerText);
    game.cps = parseFloat(document.getElementById('cps').innerText);
    game.coins += game.cps;
    document.getElementById('counterNumber').innerText = game.coins.toFixed(2).toString();
    document.getElementById('cps').innerText = game.cps.toFixed(2).toString();
}
setInterval(()=>IncreaseCookies(), 1000);


if(!localStorage.getItem('gameCoins')) {
    saveGame();
} else {
    setValues();
}

function saveGame() {
    localStorage.setItem('gameCoins', document.getElementById('counterNumber').innerText);
    localStorage.setItem('gameCps', document.getElementById('cps').innerText);

    for(let i = 0; i < game.upgrades.length; i++){
        localStorage.setItem('upgradePrice' + i.toString(), document.getElementById('upgrade' + i.toString() + 'price').innerText);
        localStorage.setItem('upgradeCount' + i.toString(), document.getElementById('upgrade' + i.toString() + 'count').innerText);
    }
   setValues();
}

function setValues() {
    let gameCoins = localStorage.getItem('gameCoins');
    let gameCps = localStorage.getItem('gameCps');
    let gameUpgradePrices = [];
    let gameUpgradeCounts = [];
    for(let i = 0; i < game.upgrades.length; i++){
        gameUpgradePrices[i] = localStorage.getItem('upgradePrice' + i.toString());
        gameUpgradeCounts[i] = localStorage.getItem('upgradeCount' + i.toString());
    }

    document.getElementById('counterNumber').innerText = gameCoins;
    document.getElementById('cps').innerText = gameCps;

    for(let i = 0; i < game.upgrades.length; i++){
        document.getElementById('upgrade' + i.toString() + 'price').innerText = gameUpgradePrices[i];
        document.getElementById('upgrade' + i.toString() + 'count').innerText = gameUpgradeCounts[i];
    }
}

setInterval(saveGame,10000);

//
// function saveGame() {
//     localStorage.setItem('savedUpgrades', JSON.stringify(game.upgrades));
//     localStorage.setItem('savedGame',JSON.stringify(game));
// }
// let savedUpgrades = localStorage.getItem('savedUpgrades');
// let savedGame = localStorage.getItem('savedGame');
//
// if (savedGame) {
//     game.upgrades = JSON.parse(savedUpgrades);
//     game = JSON.parse(savedGame);
// }

//setInterval(saveGame,10000);



