let prices = [];
let updatePrice = 0.0;
let showUpdate;
let minValue;
let currentMoney = 0.0;
function makeStringToMoney(string, money){
    if(string.includes(',')){
        money = (parseFloat(string.replace(',','.').replace(' ',''), 10));
        money *= 1000;
    } else{
    
    money = (parseFloat(string.replace(' ',''), 10));
    
    if (string.includes('million')){
       money *= Math.pow(10,6);
    }
    if (string.includes('billion')){
        money *= Math.pow(10,9);
     }
     if (string.includes('trillion')){
        money *= Math.pow(10,12);
     }
     if (string.includes('quadrillion')){
        money *= Math.pow(10,15);
     }
     if (string.includes('quintillion')){
        money *= Math.pow(10,18);
     }
    }
     return money;
}

function fakeMouseOver(el) {
    el.dispatchEvent(new MouseEvent('mouseover', new EventObject()));
    el.dispatchEvent(new MouseEvent('pointerenter', new EventObject()));
    el.dispatchEvent(new PointerEvent('pointerover', new EventObject()));
}


// Clicking BigCookie every 10 ms
setInterval(()=>getElementByXpath('//div[contains(@id, "bigCookie")]').click(), 1000);

function buyCheapestProduct (){

    //check and click the shimmer element children if any
    let shimmerElement = getElementByXpath('//div[contains(@id, "shimmers")]');

    if(shimmerElement.children.length > 0) {
        for(i = 0; i < shimmerElement.children.length; i++){
            fakeClick(shimmerElement.children[i]);
        }
    } 
    

    //collecting all products and their prices
    let products = getXpathNodesArray('//div[contains(@class, "product unlocked")]');
    let pricesString = getXpathNodesArray('//span[@class = "price"]');


    //show the update and take its price
        showUpdate = setInterval(()=> fakeMouseOver(getXpathNodesArray('//div[contains(@class, "crate upgrade")]')[0]),500);
        let priceElement = getElementByXpath('//b[contains(.,"twice") or contains(., "%") or contains(., "CpS") or contains(., "5") or contains(., "10")]/../..');
        if(priceElement != null){
        updatePrice = makeStringToMoney(priceElement.children[1].innerText, updatePrice);
        }
        //clearInterval(showUpdate);

for (var i = 0; i < products.length; i++) {
   prices[i] = makeStringToMoney(pricesString[i].innerText, prices[i]) 
 }
minValue =  Math.min(...prices);

let cookieElement = document.getElementById('cookies');
//let cookieElement = document.getElementByXpath('//div[contains(@id, "cookies")]');

    let currentMoneyString = cookieElement.innerText.split(' ')[0] + cookieElement.innerText.split(' ')[1];

    currentMoney = makeStringToMoney(currentMoneyString, currentMoney);
    
//compare update prices to product prices
    if(updatePrice < minValue && updatePrice < currentMoney){

    fakeMouseOver(getXpathNodesArray('//div[@class= "crate upgrade"]')[0]); 
    getXpathNodesArray('//div[@class= "crate upgrade"]')[0].click();
    } 
    else {
        if(currentMoney >= minValue){
    
        let minIndex = prices.indexOf(minValue);

        products[minIndex].click();
        }
    }
    products = [];
    pricesString = [];
    prices = [];
}

setInterval(buyCheapestProduct, 5000);
