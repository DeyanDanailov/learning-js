//Cyrillic to Latin Transliterator

var transliterate = function(text) {

    text = text
        .replace(/\u0401/g, 'YO')
        .replace(/\u0419/g, 'I')
        .replace(/\u0426/g, 'TS')
        .replace(/\u0423/g, 'U')
        .replace(/\u041A/g, 'K')
        .replace(/\u0415/g, 'E')
        .replace(/\u041D/g, 'N')
        .replace(/\u0413/g, 'G')
        .replace(/\u0428/g, 'SH')
        .replace(/\u0429/g, 'SCH')
        .replace(/\u0417/g, 'Z')
        .replace(/\u0425/g, 'H')
        .replace(/\u042A/g, '')
        .replace(/\u0451/g, 'yo')
        .replace(/\u0439/g, 'i')
        .replace(/\u0446/g, 'ts')
        .replace(/\u0443/g, 'u')
        .replace(/\u043A/g, 'k')
        .replace(/\u0435/g, 'e')
        .replace(/\u043D/g, 'n')
        .replace(/\u0433/g, 'g')
        .replace(/\u0448/g, 'sh')
        .replace(/\u0449/g, 'sch')
        .replace(/\u0437/g, 'z')
        .replace(/\u0445/g, 'h')
        .replace(/\u044A/g, "'")
        .replace(/\u0424/g, 'F')
        .replace(/\u042B/g, 'I')
        .replace(/\u0412/g, 'V')
        .replace(/\u0410/g, 'a')
        .replace(/\u041F/g, 'P')
        .replace(/\u0420/g, 'R')
        .replace(/\u041E/g, 'O')
        .replace(/\u041B/g, 'L')
        .replace(/\u0414/g, 'D')
        .replace(/\u0416/g, 'ZH')
        .replace(/\u042D/g, 'E')
        .replace(/\u0444/g, 'f')
        .replace(/\u044B/g, 'i')
        .replace(/\u0432/g, 'v')
        .replace(/\u0430/g, 'a')
        .replace(/\u043F/g, 'p')
        .replace(/\u0440/g, 'r')
        .replace(/\u043E/g, 'o')
        .replace(/\u043B/g, 'l')
        .replace(/\u0434/g, 'd')
        .replace(/\u0436/g, 'zh')
        .replace(/\u044D/g, 'e')
        .replace(/\u042F/g, 'Ya')
        .replace(/\u0427/g, 'CH')
        .replace(/\u0421/g, 'S')
        .replace(/\u041C/g, 'M')
        .replace(/\u0418/g, 'I')
        .replace(/\u0422/g, 'T')
        .replace(/\u042C/g, "'")
        .replace(/\u0411/g, 'B')
        .replace(/\u042E/g, 'YU')
        .replace(/\u044F/g, 'ya')
        .replace(/\u0447/g, 'ch')
        .replace(/\u0441/g, 's')
        .replace(/\u043C/g, 'm')
        .replace(/\u0438/g, 'i')
        .replace(/\u0442/g, 't')
        .replace(/\u044C/g, "'")
        .replace(/\u0431/g, 'b')
        .replace(/\u044E/g, 'yu');

    return text;
};

//Inserting new row in the table
var index = 1;

$("#new-row").on("click", function() {
    var firstname = $("#new_firstname").val();
    var lastname = $("#new_lastname").val();
    var phonenumber = $("#new_phonenumber").val();
    var email = transliterate($("#new_firstname").val()) + '_' +
                transliterate($("#new_lastname").val()) + '@abv.bg';
    $("table").html($("table").html() + 
                    "<tr id='row" + index + "'><td>" +
                    index + "<td>" + firstname + "</td><td>" +
                    lastname + "</td><td>" +
                    phonenumber + "</td><td>" + 
                    email + "</td></tr>");
  index++;
});

//Order by Columns when clicked

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("myTable2");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 2; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /* Check if the two rows should switch place,
        based on the direction, asc or desc: */
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        // Each time a switch is done, increase this count by 1:
        switchcount ++;
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  };


  function copyToClipboard(textToCopy) {
    var input = document.createElement("input");
    document.body.appendChild(input);
    input.value = textToCopy;
    input.select();
    document.execCommand("Copy");
    input.remove();
}

// function copyStringToClipboard(string) {
//   var el = document.createElement('textarea');
//   el.value = string;
//   el.setAttribute('readonly', '');
//   el.style = {position: 'absolute', left: '-9999px'};
//   document.body.appendChild(el);
//   el.select();
//   document.execCommand('copy');
//   document.body.removeChild(el);
// }

function copyColumn(td) {
    copyToClipboard(td.textContent);
    alert('copied to clipboard');
}



//   //Select cell data on double click
//   let table = document.getElementById('myTable2');

//   table.addEventListener('dblclick', (e) => {
//       let target = e.target;
      
//       if(target.localName === 'td') {
//         var copyText = target.value;
//       }
//     //       let range = document.createRange();
//     //       range.selectNodeContents(target);  
//     //       let sel= document.getSelection(); 
//     //       sel.removeAllRanges(); 
//     //       sel.addRange(range); 

//     //       document.execCommand('copy');
//     //       sel.removeAllRanges();
//     //       target.classList.add('copy-animate');
//     //       setTimeout(() => {
//     //           target.classList.remove('copy-animate');
//     //       }, 200);

//     //         /* Get the text field */
//     //         var copyText = document.getElementById("myInput");
          
//             /* Select the text field */
//             copyText.select();
//             copyText.setSelectionRange(0, 99999); /* For mobile devices */
          
//             /* Copy the text inside the text field */
//             document.execCommand("copy");
          
//             /* Alert the copied text */
//             alert("Copied the text: " + copyText.value);
          

      
//   });
