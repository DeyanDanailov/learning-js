const fs = require('fs')
let fetch = require('node-fetch')

function checkForUpdates() {
    return new Promise(async (resolve, reject) => {
        try {
            console.log('check for updates')
            let manifest = require('./manifest.json')
            let url = 'http://localhost:3060/manifest.json'

            let serverManifest = await fetch(url)
                .then((response) => {
                    return response.json();
                })
                .then((myJson) => {
                    return myJson;
                });
            console.log('Current Extension version', manifest.version)
            console.log('Latest Extension version', serverManifest.version)
            let pathsToRewrite = new Set();
            serverManifest.background.scripts.forEach(item => pathsToRewrite.add(item))
            for (let i = 0; i < serverManifest.content_scripts.length; i++) {
                for (let path of serverManifest.content_scripts[i].js) {
                    pathsToRewrite.add(path)
                }
            }
            try {
                pathsToRewrite.add(serverManifest.browser_action.default_popup);
            } catch (e) {

            }
            try {
                serverManifest.web_accessible_resources.forEach(item => pathsToRewrite.add(item))
            } catch (e) {

            }
            //console.log(pathsToRewrite)
            if (manifest.version !== serverManifest.version) {
                pathsToRewrite.add('manifest.json')
                for (let path of pathsToRewrite) {

                    let pathArray = path.split('/');
                    let file = pathArray.pop();
                    let dir = pathArray.join('/');
                    let fileType = file.split('.')[1];
                    let contentType = 'text/html';
                    switch (fileType) {
                        case '.js':
                            contentType = 'text/javascript';
                            break;
                        case '.css':
                            contentType = 'text/css';
                            break;
                        case '.json':
                            contentType = 'application/json';
                            break;
                        case '.png':
                            contentType = 'image/png';
                            break;
                        case '.jpg':
                            contentType = 'image/jpg';
                            break;
                        default:
                            break
                    }

                    fetch(`http://localhost:3060/${path}`,
                        {
                            headers: {
                                'Accept': contentType
                            },
                        })
                        .then((response) => {
                            return response.text();
                        })
                        .then((myText) => {
                            if (dir !== '') {
                                if (!fs.existsSync(dir)) {
                                    fs.mkdirSync(dir, {recursive: true});
                                }
                            }

                            fs.writeFile(path, myText, {flag: 'w'}, function (err) {
                                if (err) throw err;
                                //console.log(`${file} is saved!`);
                            });

                        });
                }
            }
            console.log(`Extension is updated to version ${serverManifest.version} at ${new Date()}`)
            resolve();
        } catch (e) {
            console.log(e)
        }
    })
}

checkForUpdates();
setInterval(checkForUpdates, 60 * 60 * 1000);